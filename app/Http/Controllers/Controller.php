<?php namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    public function view_spg()
    {
      $result = '';
      for ($i=1; $i <= 100; $i++) {
        $result.= ($i % 2 == 0) ? 'es':$i;
        $result.= ($i % 3 == 0) ? 'Pikin':'';
        $result.= ($i % 5 == 0) ? 'Scott':'';
        $result.= ($i % 3 == 0 and $i % 5 == 0) ? 'Gatica':'';
        $result.= '</br>';
      }
      return view('spg.spg', ['result' => $result]);
    }
}
