## Prueba de programación, **support programmer**

### Introducción

**Lee todo el documento antes de empezar.**

Luego de clonar el repositorio tendrás en tu computador una copia de [Lumen 5.1](http://lumen.laravel.com/), que es el framework requerido para todas las pruebas.

Cada una de las pruebas varía en complejidad y tiempo, pero todas están enmarcadas en la simplicidad.

Se debe crear una rama del repositorio con tu nombre de usuario para trabajar los ejercicios, y por cada ejercicio se debe hacer un commit. Además cada ejercicio que se pueda ver debe tener una ruta diferente; la ruta principal ya está armada y cuenta con una lista con enlaces a todos los ejercicios.

### 1. Scott, Pikin y Gatica

Scott y Pikin son perros, y los dos son enemigos, sin embargo los dos se relacionan con Gatica, que es una gata anónima sin dueño, a ninguno le parece desagradar estar en su compañía. Por lo que tendrás que imprimir los números del uno al cien y cada vez que salga un número par debes reemplazar el número con "es", cuando sea un múltiplo de tres por "Pikin", cuando sea un múltiplo de cinco por "Scott" y cuando sea un múltiplo de cinco y de tres por "Gatica". Cuando se cumplan varias condiciones se deben concatenar los resultados, por ejemplo: *30* sería "esPikinScottGatica".

### 2. Conversor de moneda API

Ya que calentamos es hora de pasar a lo verdaderamente valioso, necesitamos generar un wrapper sobre la API de [openexchangerates.org](https://openexchangerates.org/), el json que necesitamos debe ser así:

```json
{
	"timestamp": 1435089662,
	"base": "USD",
	"rates": {
		"VEF": 197.10,
		"EUR": 58.95,
		"YEN": 125.052801,
		"GBP": 473.142497,
		"CHF": 473.142497,
	}
}
```

Esta API posee un plan FREE muy limitado, que solo permite usar dólares americanos (USD) cómo base, es necesario realizar las operaciones matemáticas correspondientes para que nuestro wrapper pueda usar cualquier base que esté disponible.

### 3. Conversor de moneda UI

Hay que hacer un pequeño formulario que se comunique vía Ajax con la API, donde se pueda seleccionar cualquier moneda, ingresar un monto y ver sus cinco conversiones más populares.

Se tiene que extender la vista base incluida con el ejercicio. Esta se puede y se debe modificar y corregir si es necesario.

### 4. Maquetado simple de JPG a HTML

Tendrás que maquetar una imagen que se encuentra en la ruta *resources/designs/* que consiste de un simple sitio de e-commerce. La página debe ser visualmente funcional, pero no se requiere que se pueda interactuar (hacer zoom a las imágenes, loguearse, comprar...).

### 5. Agregar favicon desde logo `.svg`

Existe un archivo de imagen formato `.svg` en la misma ruta del diseño con el que se debe hacer el favicon de la página, este debe incluir todos los tamaños y especificaciones recomendados para todos los dispositivos y usos.

### 6. Esquemas de base de datos

Ya que tenemos el template armado, es necesario que dejemos a un lado lo estático y hagamos que nuestro sitio de e-commerce se alimente de una base de datos.

El ejercicio consiste en crear las tablas necesarias para darle funcionalidad, llenar cada tabla con datos de prueba y hacer que el template use esos datos.

### 7. Buy

El último ejercicio consiste en hacer que cada botón de compra (Buy) alimente al indicador de precio en el header (en USD), al hacer click en el botón de *checkout* un modal debe indicarnos cuantos productos compramos, a que precio y cual es su precio estimado en VEF (usando el API que desarrollamos al inicio).

En el modal tendremos un botón para terminar la compra, esto enviará un correo electrónico con la información de compra a *axel.grazx@gmail.com* y *ajqb1991@gmail.com*, pero solo para la versión final, en local prueben con su correo electrónico.

Los datos no necesitan ser persistentes, es decir, no hay problemas si al actualizar la página el contador de artículos vuelve a ser cero.

### Otra información de interés

Si posees alguna duda de la prueba puedes enviarnos un correo electrónico y trataremos de responderte a la brevedad.

No hay muchas reglas, pero sí hay ciertos detalles a tomar en cuenta:

- Valoraremos el estilo del código, su apego a los estándares y su uso del flujo de control.
- Tienes la libertad de usar cualquier herramienta que desees, apreciamos mucho más que uses algo probado y con retroalimentación de la comunidad a que reinventes la rueda.
- No hay un límite de tiempo de la prueba, pero un rango de un día debería bastar.
- El uso eficiente de GIT y *commits* representativos serán valorados.
- Lumen es un micro-framework con mucho poder, puedes y debes usar lo que tengas disponible para hacer más fácil tu trabajo, por ejemplo agregarle Elixir para usar Gulp (SCSS/LESS/CoffeeScript).
- Usamos Bootstrap con SCSS, así que valoraremos si se usa en la prueba.
- Hay muchas secciones de la prueba con poco detalle, esto es intencional. Ya que no es un producto real, tienes potestad para diseñar como te lo imagines.
- Solo en esta prueba vamos a solicitar que los *key* de las API de los servicios utilizados, sean incluidos en el repositorio, esto dicho, no limita un uso correcto de las variables de entorno.

Al final de la prueba, envía un *pull request* de tu rama y entrará en fase de revisión.