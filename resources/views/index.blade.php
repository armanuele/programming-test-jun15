@extends('layouts.base')

@section('main')
	<div class="container container-sunas">
		<h1>Sunas</h1>
		<h2>Programming test (JUN 2015)</h2>

		<ul class="list-unstyled">
			<li><a href="{{url('spg')}}">1. Scott, Pikin y Gatica</a></li>
			<li><a href="#">2. Conversor de moneda API</a></li>
			<li><a href="#">3. Conversor de moneda UI</a></li>
			<li><a href="#">4. Maquetado simple de JPG a HTML</a></li>
		</ul>
	</div>
@stop